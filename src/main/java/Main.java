import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    public static void main(String[] args) throws IOException {
        String accessToken = args[0];
        String account = args[1];

        String transactionUrl = "https://api.monzo.com/transactions?expand[]=merchant";


        try (Response response = new OkHttpClient().newCall(new Request.Builder()
                .url(HttpUrl.parse(transactionUrl).newBuilder()
                        .addQueryParameter("since", "2019-09-01T0:00:00Z")
                        .addQueryParameter("account_id", account)
                        .build())
                .header("Authorization", "Bearer " + accessToken)
                .build()).execute()) {
            JsonNode jsonNode = new ObjectMapper().readTree(response.body().byteStream());
            FileWriter fileWriter = new FileWriter("./export.csv");
            String header = "Date,Reference,Amount,Balance\n";
            fileWriter.write(header);
            System.out.print(header);
            AtomicInteger runningBalance = new AtomicInteger(0);
            jsonNode.get("transactions").elements().forEachRemaining(transaction -> {

                // ignore anything to do with pots, they aren't bank accounts
                if ("uk_business_pot".equals(transaction.get("scheme").asText())) {
                    return;
                }

                String date = LocalDate.parse(transaction.get("created").asText().split("T")[0]).format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
                String ref = transaction.get("description").asText();
                int amount1 = transaction.get("amount").asInt();

                JsonNode counterparty = transaction.get("counterparty");
                if (counterparty.has("name")) {
                    ref = counterparty.get("name").asText() + ": " + ref;
                }

                // skip anything which has a zero balance
                if (amount1 == 0) {
                    return;
                }

                String amount = convertPenceToPounds(amount1);
                String balance = "" + convertPenceToPounds(runningBalance.addAndGet(amount1));
                String join = String.join(",", date, ref, amount, balance) + "\n";
                try {
                    fileWriter.write(join);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                System.out.print(join);
            });
            fileWriter.close();
        }
    }

    private static String convertPenceToPounds(int amount1) {
        return new BigDecimal(amount1).divide(ONE_HUNDRED, 2, RoundingMode.UNNECESSARY).toPlainString();
    }
}
