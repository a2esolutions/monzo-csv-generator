# Monzo CSV Generator

Simple java class to convert Monzo transactions into a Crunch compatible CSV file.

To run:

``mvn clean install``

``cd target``

``java -jar monzo-csv-generator.jar <<accesstoken>> <<account_id>>``

Alternatively, use IntelliJ or equivalent and run Main.java using 2 arguments as above.

## Access Token
You can obtain an access token from here:

https://developers.monzo.com/

Go to the API playground to get your account ID and access token.


## Output

The output is a CSV file with the headings:

``Date,Reference,Amount,Balance``

It should appear in the same directory as this readme when you run the program.

